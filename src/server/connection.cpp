#include <fstream>

#include "connection.h"

std::pair<std::pair<long long, std::string>, std::string> ConnectionServer::recieve_meta_data()
{
    std::pair<std::pair<long long, std::string>, std::string> result;

    const auto filename = read_message_with_response();
    if (!filename.second.empty())
    {
        result.second = filename.second;
        return result;
    }

    const auto file_size = read_message_with_response();
    if (!file_size.second.empty())
    {
        result.second = file_size.second;
        return result;
    }

    try
    {
        result.first.first = std::stoll(file_size.first);
    }
    catch (std::exception& ex)
    {
        result.second = ex.what();
        return result;
    }
    result.first.second = filename.first;

    return result;
}

std::string ConnectionServer::session() noexcept
{
    std::cout << "open client session" << std::endl;

    update_last_ping();

    std::string status_message = "success data recieving";

    const std::string error_prefix = "finishing data recieving by error: ";

    // at first we recieve filename and file size
    const auto meta_data = recieve_meta_data();
    if (!meta_data.second.empty())
    {
        return error_prefix + meta_data.second;
    }
    const auto& meta = meta_data.first;

    std::cout << "start recieving file " << meta.second << " with size " << meta.first << std::endl;

    size_t bytes_recieved = 0;

    size_t bytes_remaining = meta.first;

    std::ofstream file;

    try
    {
        file.open(meta.second);
    }
    catch (const std::exception& e)
    {
        return error_prefix + " error open file " + meta.second;
    }

    while (bytes_remaining > 0 && !m_termination)
    {
        const auto data = read_message_with_response(std::min(bytes_remaining, buffer_size));

        if (!data.second.empty())
        {
            return error_prefix + data.second;
        }
        const auto now_recieved = data.first.size();

        bytes_recieved += now_recieved;

        try
        {
            file.write(data.first.c_str(), now_recieved);
        }
        catch (const std::exception& e)
        {
            return error_prefix + " error file write: " + e.what();
        }

        bytes_remaining = meta.first - bytes_recieved;
        std::cout << "transferred " << bytes_recieved << " bytes\r";
    }
    std::cout << std::endl;

    status_message += '\n';

    auto err = write_full_message(status_message.c_str(), status_message.size());

    if (err)
    {
        status_message += ", error write finishing status message to client: " + err.message();
    }

    return "close client session with server message: " + status_message;
}
