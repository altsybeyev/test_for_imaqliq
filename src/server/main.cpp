#include <iostream>

#include <boost/asio/signal_set.hpp>
#include <boost/thread.hpp>

#include "connection.h"

int main()
{
    boost::asio::io_service        service;
    boost::asio::ip::tcp::endpoint ep(boost::asio::ip::tcp::v4(), 112);
    boost::thread_group            threads;
    boost::mutex                   mutex;

    try
    {
        boost::asio::ip::tcp::acceptor acc(service, ep);

        boost::asio::signal_set signals(service, SIGINT, SIGTERM, SIGHUP);

        std::set<std::shared_ptr<ConnectionServer>> connections;

        std::function<void(std::shared_ptr<ConnectionServer>, const boost::system::error_code& err)> accept =
            [&connections, &service, &acc, &accept, &mutex](std::shared_ptr<ConnectionServer> connection,
                                                            const boost::system::error_code&  err) {
                mutex.lock();
                connections.insert(connection);

                std::cout << "add new connection, currently " << connections.size() << std::endl;
                mutex.unlock();

                service.post([connection, &connections, &mutex]() {
                    std::cout << connection->session() << std::endl;
                    boost::lock_guard<boost::mutex> lk(mutex);
                    connections.erase(connection);
                });
                auto new_connection = std::make_shared<ConnectionServer>(service);
                acc.async_accept(
                    new_connection->socket(),
                    [new_connection, &accept](const boost::system::error_code& err) { accept(new_connection, err); });
            };

        auto connection = std::make_shared<ConnectionServer>(service);

        acc.async_accept(connection->socket(),
                         [connection, &accept](const boost::system::error_code& err) { accept(connection, err); });

        signals.async_wait([&connections, &service, &mutex](const boost::system::error_code& err, int signal) {
            std::cout << "terminate by signal" << std::endl;
            mutex.lock();
            for (auto&& connection : connections)
            {
                connection->terminate();
            }
            mutex.unlock();

            service.stop();
            exit(1);
        });

        const size_t n_threads = 10;

        for (size_t i = 0; i < n_threads; i++)
        {
            threads.create_thread([&]() { service.run(); });
        }

        threads.join_all();
    }
    catch (boost::system::system_error e)
    {
        std::cout << "error: " << e.what() << std::endl;
    }
    std::cout << "finish";

    return 0;
}
