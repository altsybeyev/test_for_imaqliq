#ifndef CONNECTIONSERVER_H
#define CONNECTIONSERVER_H

#include "common/common.h"

class ConnectionServer : public ConnectionBase
{

  public:
    using ConnectionBase::ConnectionBase;

    std::pair<std::pair<long long, std::string>, std::string> recieve_meta_data();

    std::string session() noexcept;
};

#endif
