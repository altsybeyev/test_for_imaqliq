#ifndef CONNECTIONCLIENT_H
#define CONNECTIONCLIENT_H

#include <boost/asio.hpp>

#include "common/common.h"

class ConnectionClient : public ConnectionBase
{
  public:
    ~ConnectionClient();

    ConnectionClient(boost::asio::io_service& service, boost::asio::ip::tcp::endpoint& ep, const std::string& file_path);

    std::string session() noexcept;

  private:
    static size_t get_file_size(std::FILE* file_pointer);

    std::string transfer_data();

    std::string transfer_meta_data();

    std::FILE*  m_file_pointer;
    size_t      m_file_size;
    std::string m_file_path;
};

#endif
