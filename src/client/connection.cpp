#include <boost/filesystem.hpp>

#include "connection.h"

ConnectionClient::ConnectionClient(boost::asio::io_service& service, boost::asio::ip::tcp::endpoint& ep,
                                   const std::string& file_path)
    : ConnectionBase(service), m_file_pointer(std::fopen(file_path.c_str(), "rb")), m_file_path(file_path)
{
    m_socket.connect(ep);
    std::cout << "Success connection to server" << std::endl;

    if (!m_file_pointer)
    {
        throw std::runtime_error("error open input file " + file_path);
    }

    m_file_size = get_file_size(m_file_pointer);
}

ConnectionClient::~ConnectionClient()
{
    std::fclose(m_file_pointer);
}

std::string ConnectionClient::session() noexcept
{
    std::cout << "open server session" << std::endl;

    const std::string error_prefix = "close server session by error: ";

    update_last_ping();

    std::cout << "transfer file metadata" << std::endl;
    const auto err_meta = transfer_meta_data();
    if (!err_meta.empty())
    {
        return error_prefix + err_meta;
    }

    std::cout << "transfer file data" << std::endl;
    const auto err_transfer = transfer_data();
    if (!err_transfer.empty())
    {
        return error_prefix + err_transfer;
    }

    const auto server_status_msg = read_full_message('\n');

    if (!server_status_msg.second.empty())
    {
        return error_prefix + server_status_msg.second;
    }
    return "close server session with server message: " + server_status_msg.first;
}

size_t ConnectionClient::get_file_size(std::FILE* file_pointer)
{
    std::fseek(file_pointer, 0, SEEK_END);
    size_t file_size = std::ftell(file_pointer);
    std::rewind(file_pointer);

    return file_size;
}

std::string ConnectionClient::transfer_data()
{
    size_t total_bytes = 0;
    while (!std::feof(m_file_pointer))
    {
        auto readed_bytes = std::fread(m_buffer.data(), sizeof(char), buffer_size, m_file_pointer);

        if (readed_bytes != buffer_size)
        {
            if (std::ferror(m_file_pointer))
            {
                return "error read file";
            }
        }

        auto err1 = write_message_with_response(m_buffer.data(), readed_bytes);
        if (!err1.second.empty())
        {
            return err1.second;
        }
        total_bytes += readed_bytes;
        std::cout << "transferred " << total_bytes << " bytes\r";
    }
    std::cout << std::endl;

    return "";
}

std::string ConnectionClient::transfer_meta_data()
{
    boost::filesystem::path p(m_file_path);

    const std::string filename  = p.filename().string() + '\n';
    const std::string file_size = std::to_string(m_file_size) + '\n';

    const auto err = write_message_with_response(filename.c_str(), filename.size());
    if (!err.second.empty())
    {
        return err.second;
    }

    auto err1 = write_message_with_response(file_size.c_str(), file_size.size());
    if (!err1.second.empty())
    {
        return err1.second;
    }
    return "";
}
