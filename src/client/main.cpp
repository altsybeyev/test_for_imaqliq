
#include "connection.h"

int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        std::cout << "error: incorrect number of arguments" << std::endl;
        return 0;
    }

    boost::asio::io_service service;

    try
    {
        boost::asio::ip::tcp::endpoint ep(boost::asio::ip::address::from_string(argv[1]), 112);
        ConnectionClient connection(service, ep, argv[2]);
        std::cout << connection.session() << std::endl;
    }
    catch (boost::system::system_error e)
    {
        std::cout << "error: " << e.what() << std::endl;
    }

    return 0;
}