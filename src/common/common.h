#ifndef COMMOH_H
#define COMMOH_H

#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

class ConnectionBase
{
  public:
    ConnectionBase(boost::asio::io_service& service);

    boost::asio::ip::tcp::socket& socket();

    virtual ~ConnectionBase();

    void terminate() noexcept;

  protected:
    boost::system::error_code write_full_message() noexcept;

    boost::system::error_code write_full_message(const char* data, const size_t size) noexcept;

    // send message and await responce. return pair of message and error
    std::pair<std::string, std::string> write_message_with_response(const char* data, const size_t size);

    // read message and send responce. return pair of message and error
    std::pair<std::string, std::string> read_message_with_response(const long expected_lenght = 0);

    bool check_condition(size_t& len, const char end_symbol);

    bool check_condition(size_t& len, const long expected_lenght);

    template <class T> std::pair<std::string, std::string> read_full_message(const T& arg) noexcept;

    bool timed_out() const;
    void update_last_ping();

    boost::posix_time::ptime m_last_ping;

    const static constexpr size_t buffer_size = 1024;

    const static constexpr size_t time_out = 5000;

    std::array<char, buffer_size> m_buffer;

    boost::asio::ip::tcp::socket m_socket;

    std::atomic<bool> m_termination;
};

#endif