#include "common.h"

ConnectionBase::ConnectionBase(boost::asio::io_service& service) : m_socket(service), m_termination(false)
{
    update_last_ping();
}

boost::asio::ip::tcp::socket& ConnectionBase::socket()
{
    return m_socket;
}

ConnectionBase::~ConnectionBase()
{
    m_socket.close();
}

void ConnectionBase::terminate() noexcept
{
    m_termination = true;
}

boost::system::error_code ConnectionBase::write_full_message() noexcept
{
    return write_full_message(m_buffer.data(), buffer_size);
}

boost::system::error_code ConnectionBase::write_full_message(const char* data, const size_t size) noexcept
{
    boost::system::error_code ec;
    boost::asio::write(m_socket, boost::asio::buffer(data, size), ec);
    return ec;
}

std::pair<std::string, std::string> ConnectionBase::write_message_with_response(const char* data, const size_t size)
{
    std::pair<std::string, std::string> result;

    auto err = write_full_message(data, size);

    if (err)
    {
        result.second = err.message();
        return result;
    }

    const auto responce = read_full_message('\n');

    if (!responce.second.empty())
    {
        result.second = responce.second;
        return result;
    }

    result.first = responce.first;

    return result;
}

std::pair<std::string, std::string> ConnectionBase::read_message_with_response(const long expected_lenght)
{
    std::pair<std::string, std::string> result;

    if (expected_lenght == 0)
    {
        result = read_full_message<char>('\n');
    }
    else
    {
        result = read_full_message<long>(expected_lenght);
    }

    if (!result.second.empty())
    {
        return result;
    }
    std::string resp = "ping\n";

    auto err = write_full_message(resp.c_str(), resp.size());

    if (err)
    {
        result.second = err.message();
        return result;
    }

    return result;
}

bool ConnectionBase::check_condition(size_t& len, const char end_symbol)
{
    if (end_symbol == m_buffer[len - 1])
    {
        len--;
        return true;
    }
    return len == buffer_size;
}

bool ConnectionBase::check_condition(size_t& len, const long expected_lenght)
{
    return len == expected_lenght;
}

template <class T> std::pair<std::string, std::string> ConnectionBase::read_full_message(const T& arg) noexcept
{
    size_t len = 0;

    std::pair<std::string, std::string> result;

    boost::system::error_code ec;

    while (true)
    {
        if (timed_out())
        {
            result.second = "time out";
            return result;
        }

        if (m_socket.available())
        {
            len += m_socket.read_some(boost::asio::buffer(m_buffer.data() + len, buffer_size - len), ec);

            update_last_ping();

            if (ec)
            {
                result.second = ec.message();
                return result;
            }
            if (check_condition(len, arg))
            {
                break;
            }
        }
    }
    result.first = std::string(m_buffer.data(), len);
    return result;
}

bool ConnectionBase::timed_out() const
{
    const auto now = boost::posix_time::microsec_clock::universal_time();
    long long  ms  = (now - m_last_ping).total_milliseconds();
    return ms > time_out;
}

void ConnectionBase::update_last_ping()
{
    m_last_ping = boost::posix_time::microsec_clock::universal_time();
}